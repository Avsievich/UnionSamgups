package ru.samgups.union.novosti.model;

import org.springframework.format.annotation.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
public class CurrentEventsEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  long id;
    @Column (name = "headlineNews")
    private String headlineNews;
    @Column (name = "theNewsInShort")
    private  String theNewsInShort;
    @Column (name = "news", columnDefinition="TEXT")
    private  String news;
    @Column (name = "newsDate")
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date newsDate;
    @OneToMany (fetch = FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="currentEventsEntity")
    List <PhotoNewsEntity> photoNews;


    public CurrentEventsEntity() {
    }

    public CurrentEventsEntity(String headlineNews, String theNewsInShort, String news, Date newsDate, List<PhotoNewsEntity> photoNews) {
        this.headlineNews = headlineNews;
        this.theNewsInShort = theNewsInShort;
        this.news = news;
        this.newsDate = newsDate;
        this.photoNews = photoNews;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHeadlineNews() {
        return headlineNews;
    }

    public void setHeadlineNews(String headlineNews) {
        this.headlineNews = headlineNews;
    }

    public String getTheNewsInShort() {
        return theNewsInShort;
    }

    public void setTheNewsInShort(String theNewsInShort) {
        this.theNewsInShort = theNewsInShort;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public Date getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(Date newsDate) {
        this.newsDate = newsDate;
    }

    public List<PhotoNewsEntity> getPhotoNews() {
        return photoNews;
    }

    public void setPhotoNews(List<PhotoNewsEntity> photoNews) {
        this.photoNews = photoNews;
    }

    @Override
    public String toString() {
        return String.format(" id ='%s,'iconImageUrl='%s', website='%s', title='%s', desscriptio='%s']",  id, headlineNews,theNewsInShort,news);
    }
}
