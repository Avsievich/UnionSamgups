package ru.samgups.union.novosti.model;

import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

public class CurrentUploadFormEvents {

    private long id;

    private String description;

    // Upload files.
    private MultipartFile[] fileDatas;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile[] getFileDatas() {
        return fileDatas;
    }

    public void setFileDatas(MultipartFile[] fileDatas) {
        this.fileDatas = fileDatas;
    }

}
