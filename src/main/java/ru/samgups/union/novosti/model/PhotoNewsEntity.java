package ru.samgups.union.novosti.model;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Entity
public class PhotoNewsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
   private long id;

  private   String photoURL;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn (name="currentEventsEntity_id")
    CurrentEventsEntity currentEventsEntity;

    public PhotoNewsEntity(){

    }

    public PhotoNewsEntity(String photoURL, CurrentEventsEntity currentEventsEntity) {
        this.photoURL = photoURL;

        this.currentEventsEntity = currentEventsEntity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }



    public CurrentEventsEntity getCurrentEventsEntity() {
        return currentEventsEntity;
    }

    public void setCurrentEventsEntity(CurrentEventsEntity currentEventsEntity) {
        this.currentEventsEntity = currentEventsEntity;
    }
}
