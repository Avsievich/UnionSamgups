package ru.samgups.union.novosti.service;


import groovy.lang.Singleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.samgups.union.novosti.model.CurrentEventsEntity;
import ru.samgups.union.novosti.persistence.CurrentEventsWabsiteRepositoty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service

public class CurrentEventsService {

    @Autowired
    CurrentEventsWabsiteRepositoty CurrentEvents;

    // Выбираем из бызы все новости
    public Iterable<CurrentEventsEntity> findAll() {

        return CurrentEvents.findAll();
    }

    // Выбираем из бызы выбранную новость
    public Iterable<CurrentEventsEntity> findAll(Long id) {
        List<CurrentEventsEntity> news = new ArrayList<CurrentEventsEntity>();
        for (CurrentEventsEntity currentEventsEntity : CurrentEvents.findAll(Collections.singleton(id))) {
            news.add(currentEventsEntity);
        }
        return news;

    }


    // Сохроняем Новость
    public CurrentEventsEntity save(CurrentEventsEntity iterable) {
        return CurrentEvents.save(iterable);
    }



    // Выбираем из бызы выбранную новость, ленивый перебор (выдоет данные по порциям)
    public Iterable<CurrentEventsEntity> findAllB(int fromIndex, int toIndex) {
        List<CurrentEventsEntity> news = new ArrayList<CurrentEventsEntity>();
        for (CurrentEventsEntity currentEventsEntity : CurrentEvents.findAll()) {
            news.add(currentEventsEntity);
        }
        return safeSubList(news, fromIndex, toIndex);
    }



// Формируем массив новостей для построничного вывода
    public static <T> List<T> safeSubList(List<T> list, int fromIndex, int toIndex) {
        int size = list.size();
        if (fromIndex >= size || toIndex <= 0 || fromIndex >= toIndex) {
            return Collections.emptyList();
        }
        fromIndex = Math.max(0, fromIndex);
        toIndex = Math.min(size, toIndex);

        return list.subList(fromIndex, toIndex);
    }
}
