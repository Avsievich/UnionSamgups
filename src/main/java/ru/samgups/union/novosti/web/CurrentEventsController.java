package ru.samgups.union.novosti.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import ru.samgups.union.novosti.model.CurrentEventsEntity;
import ru.samgups.union.novosti.model.CurrentUploadFormEvents;
import ru.samgups.union.novosti.model.PhotoNewsEntity;
import ru.samgups.union.novosti.service.CurrentEventsService;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


@Controller
public class CurrentEventsController {

    //Количество новостей на странице
    private final int cardinality = 3;
    //Начальный диапозон выборки новостей
    private final int fromIndex = 0;
    //Конечный  диапозон выборки новостей
    private final int toIndex = 3;


    @Autowired
    CurrentEventsService currentEventsService;

    // Выводим новости
    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcomePage(Model model) {

        model.addAttribute("title", "Новости профсоюза");
        model.addAttribute("novost", getNovostFI(0));
        model.addAttribute("photos", filesName());
        model.addAttribute("Pages", arrayOfPages(cardinality));

        return "welcomePage";
    }


    // Выводим новостей по и ID
    @RequestMapping(value = {"/welcome/{papesid}", "/{papesid}"}, method = RequestMethod.GET)
    public String welcomePage(Model model, @PathVariable Integer papesid) {


        model.addAttribute("title", "Новости профсоюза");
        model.addAttribute("novost", getNovostFI(papesid));
        model.addAttribute("photos", filesName());
        model.addAttribute("Pages", arrayOfPages(cardinality));
        return "welcomePage";
    }

    // Выводим новость
    @RequestMapping(value = {"/news/{itemid}"}, method = RequestMethod.GET)
    public String news(Model model, @PathVariable Long itemid) {
        List<CurrentEventsEntity> masNews = new ArrayList<>();
        List<String> files = new ArrayList<>();
        masNews = getNews(itemid);
        for (CurrentEventsEntity current : masNews) {
            for (PhotoNewsEntity photoNewsEntity : current.getPhotoNews()
                    ) {
                files.add(photoNewsEntity.getPhotoURL());
            }
        }
        model.addAttribute("title", "Новость");
        model.addAttribute("news", masNews);
        model.addAttribute("photos", files);
        model.addAttribute("itemid", itemid);
        return "novosti/newsPage";
    }


    // Формируем массив новостей
    private List<CurrentEventsEntity> getNovost() {
        List<CurrentEventsEntity> novost = new ArrayList<CurrentEventsEntity>();
        for (CurrentEventsEntity currentEventsEntity : currentEventsService.findAll()
                ) {
            novost.add(currentEventsEntity);
        }
        return novost;
    }

    // Формируем новость
    private List<CurrentEventsEntity> getNews(Long itemid) {
        List<CurrentEventsEntity> news = new ArrayList<CurrentEventsEntity>();
        for (CurrentEventsEntity currentEventsEntity : currentEventsService.findAll(itemid)
                ) {
            news.add(currentEventsEntity);
        }
        return news;
    }

    // Формируем Списка фотографий для новостей
    private List<String> filesName() {
        List<String> files = new ArrayList<>();
        for (CurrentEventsEntity current : getNovost()) {
            for (PhotoNewsEntity photoNewsEntity : current.getPhotoNews()
                    ) {
                files.add(photoNewsEntity.getPhotoURL());
            }
        }
        return files;
    }

    //Возфрошаем количество страниц << 1 2 3....>>
    private List<Integer> arrayOfPages(int cardinality) {
        int numberOfPages = (getNovost().size() / cardinality);
        List<Integer> arrayOfPages = new ArrayList<>();
        for (int i = 0; i <= numberOfPages; i++) {
            arrayOfPages.add(i);
        }
        return arrayOfPages;
    }


    //Возфрошаем массив новостей в диапозоне fromIndex toIndex

    private Iterable<CurrentEventsEntity> getNovostFI(Integer papesid) {

        Iterable<CurrentEventsEntity> getNovostB = new ArrayList<>();

        //Начальный диапозон выборки новостей
        int fromIndexFI = fromIndex;
        //Конечный  диапозон выборки новостей
        int toIndexFI = toIndex;

        if (papesid == null || papesid == 0) {

        } else {
            for (int i = 1; i <= papesid; i++) {
                fromIndexFI = fromIndexFI + cardinality;
                toIndexFI= toIndexFI + cardinality;
            }
        }
        getNovostB = currentEventsService.findAllB(fromIndexFI, toIndexFI);
        return getNovostB;
    }


}
