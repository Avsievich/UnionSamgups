package ru.samgups.union.novosti.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import ru.samgups.union.novosti.model.CurrentEventsEntity;
import ru.samgups.union.novosti.model.CurrentUploadFormEvents;
import ru.samgups.union.novosti.model.PhotoNewsEntity;
import ru.samgups.union.novosti.service.CurrentEventsService;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


@Controller
public class NewsNewController {

    @Autowired
    CurrentEventsService currentEventsService;

    //    Cоздоем новой обьект новости, и заполняем его
    @RequestMapping(value = "/newsediting/newsadded", method = RequestMethod.GET)
    public String greetingForm(Model model) {
        //model.addAttribute("newNews", new CurrentEventsEntity());
        CurrentUploadFormEvents currentUploadFormEvents = new CurrentUploadFormEvents();
        model.addAttribute("newNews", newNews());
        model.addAttribute("uploadFormEvents", currentUploadFormEvents);
        return "novosti/newNewsPage";
    }



    //Сохраняем новость и Вывести инвормацию о том что новость сохранина
    @RequestMapping(value = "/newsediting/addnews", method = RequestMethod.POST)
    public String greetingSubmit(CurrentEventsEntity newNews, BindingResult bindingResult, Model model, HttpServletRequest request,
                                 @ModelAttribute("myUploadForm") CurrentUploadFormEvents myUploadForm) {
        //   model.addAttribute("newNews", currentEventsService.save(newNews));
        if (bindingResult.hasErrors()) {
            return "form";
        }
        List<PhotoNewsEntity> photoNews = new ArrayList<>();
        List<String> uploadedFiles = doUpload(request, model, myUploadForm);
        for (String file : uploadedFiles) {
            PhotoNewsEntity photoNewsEntity = new PhotoNewsEntity();
            photoNewsEntity.setPhotoURL(file);
            photoNewsEntity.setCurrentEventsEntity(newNews);
            photoNews.add(photoNewsEntity);
        }
        newNews.setPhotoNews(photoNews);
        newNewsSave(newNews);
        model.addAttribute("news", newNews);
        return "novosti/infoNewNewsPage";
    }


    //Сохроняем новости в БД
    public void newNewsSave(CurrentEventsEntity save) {
        currentEventsService.save(save);
    }

    // Создаем обьект новость
    public CurrentEventsEntity newNews() {
        CurrentEventsEntity newNews = new CurrentEventsEntity();
        return newNews;
    }


    /// Сохроняем фотографии на сервер и передаем соллекцию с именами файлов
    private List<String> doUpload(HttpServletRequest request, Model model, CurrentUploadFormEvents myUploadForm) {
        // Linux: /home/{user}/test
        // Windows: C:/Users/{user}/test
       String rootPath = "D:\\path\\";

//       String rootPath = "/imeges";
//        String description = myUploadForm.getDescription();
//        System.out.println("Description: " + description);

        // Root Directory.
//        String uploadRootPath = request.getServletContext().getRealPath("upload");
//        System.out.println("uploadRootPath=" + uploadRootPath);

 //       File uploadRootDir = new File(uploadRootPath);
        // Create directory if it not exists.
   //     if (!uploadRootDir.exists()) {
  //          uploadRootDir.mkdirs();
  //      }
        MultipartFile[] fileDatas = myUploadForm.getFileDatas();
        //
        List<File> uploadedFiles = new ArrayList<File>();
        List<String> failedFiles = new ArrayList<String>();
        List<String> pathFiles = new ArrayList<String>();
        for (MultipartFile fileData : fileDatas) {

            // Client File Name
            String name = fileData.getOriginalFilename();
            System.out.println("Client File Name = " + name);

            if (name != null && name.length() > 0) {
                try {
                    // Create the file at server
                    File serverFile = new File(rootPath + File.separator + name);

                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(fileData.getBytes());
                    stream.close();
                    //
                    uploadedFiles.add(serverFile);
//                    pathFiles.add(serverFile.getAbsolutePath());
                    pathFiles.add(name);
                    System.out.println("Write file: " + serverFile);
                } catch (Exception e) {
                    System.out.println("Error Write file: " + name);
                    failedFiles.add(name);
                }
            }
        }
        
    //    model.addAttribute("description", description);
//        model.addAttribute("uploadedFiles", uploadedFiles);
//        model.addAttribute("failedFiles", failedFiles);

        return pathFiles;
    }

}
