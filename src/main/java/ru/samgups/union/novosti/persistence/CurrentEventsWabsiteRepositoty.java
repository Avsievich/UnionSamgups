package ru.samgups.union.novosti.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.samgups.union.novosti.model.CurrentEventsEntity;

@Repository
public interface CurrentEventsWabsiteRepositoty extends CrudRepository<CurrentEventsEntity, Long> {

    Iterable<CurrentEventsEntity> findAllBy(Long id);


  //  CurrentEventsEntity  save( CurrentEventsEntity iterable);
}
