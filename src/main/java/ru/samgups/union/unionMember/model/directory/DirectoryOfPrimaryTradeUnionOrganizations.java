package ru.samgups.union.unionMember.model.directory;


import ru.samgups.union.unionMember.model.UnionMemberCard;

import javax.persistence.*;

// Справочник Первичных Профсоюзных Организаций
@Entity
@Table(name = "PrimaryTradeUnionOrganizations")
public class DirectoryOfPrimaryTradeUnionOrganizations {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;

    @OneToOne(optional = false, mappedBy = "directoryOfPrimaryTradeUnionOrganizations")
    @JoinColumn (name="OfPrimaryTradeUnionOrganizationsId")
    UnionMemberCard unionMemberCard;

    public DirectoryOfPrimaryTradeUnionOrganizations() {
    }

    public DirectoryOfPrimaryTradeUnionOrganizations(long id, String name, UnionMemberCard unionMemberCard) {
        this.id = id;
        this.name = name;
        this.unionMemberCard = unionMemberCard;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UnionMemberCard getUnionMemberCard() {
        return unionMemberCard;
    }

    public void setUnionMemberCard(UnionMemberCard unionMemberCard) {
        this.unionMemberCard = unionMemberCard;
    }
}
