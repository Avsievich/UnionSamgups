package ru.samgups.union.unionMember.model;


import org.springframework.format.annotation.DateTimeFormat;
import ru.samgups.union.unionMember.model.directory.DepartmentDirectoryEntity;
import ru.samgups.union.unionMember.model.directory.DirectoryOfIndividualsEntity;
import ru.samgups.union.unionMember.model.directory.DirectoryOfPrimaryTradeUnionOrganizations;
import ru.samgups.union.unionMember.model.directory.DirectoryOfProfessionsEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "UnionMemberCard")
public class UnionMemberCard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @Column(name = "unionTicketNumber")
    private long unionTicketNumber;
    @Column(name = "tradeUnionCard")
    private long e_tradeUnionCard;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @Column(name = "dateOfJoiningTheUnion")
    private Date dateOfJoiningTheUnion;
    @Column(name = "EPRN")
    private long EPRN;
    @Column(name = "officePhone")
    private long officePhone;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @Column(name = "dateOfWithdrawalFrom")
    private Date dateOfWithdrawalFromTheUnion;
    @Column(name = "groundsForWithdrawal")
    private String groundsForWithdrawalFromTheUnion;
    @Column(name = "isNotAMemberOfATrade")
    private boolean isNotAMemberOfATradeUnion;

    @OneToOne(optional = false, cascade = CascadeType.ALL )
    @JoinColumn(name = "idUnionMemberCardOfIndividuals")
    DirectoryOfIndividualsEntity directoryOfIndividualsEntity;


    @OneToOne(optional = false, cascade = CascadeType.ALL )
    @JoinColumn(name = "idUnionMemberCardDirectory")
    DepartmentDirectoryEntity departmentDirectoryEntity;

    @OneToOne(optional = false, cascade = CascadeType.ALL )
    @JoinColumn(name = "UnionMemberCardTradeUnion")
    DirectoryOfPrimaryTradeUnionOrganizations directoryOfPrimaryTradeUnionOrganizations;

    @OneToOne(optional = false, cascade = CascadeType.ALL )
    @JoinColumn(name = "UnionMemberCardOfProfessions")
    DirectoryOfProfessionsEntity directoryOfProfessionsEntity;

    public UnionMemberCard() {
    }

    public UnionMemberCard(int id, long unionTicketNumber, long e_tradeUnionCard, Date dateOfJoiningTheUnion, long EPRN, long officePhone, Date dateOfWithdrawalFromTheUnion, String groundsForWithdrawalFromTheUnion, boolean isNotAMemberOfATradeUnion, DirectoryOfIndividualsEntity directoryOfIndividualsEntity, DepartmentDirectoryEntity departmentDirectoryEntity, DirectoryOfPrimaryTradeUnionOrganizations directoryOfPrimaryTradeUnionOrganizations, DirectoryOfProfessionsEntity directoryOfProfessionsEntity) {
        this.id = id;
        this.unionTicketNumber = unionTicketNumber;
        this.e_tradeUnionCard = e_tradeUnionCard;
        this.dateOfJoiningTheUnion = dateOfJoiningTheUnion;
        this.EPRN = EPRN;
        this.officePhone = officePhone;
        this.dateOfWithdrawalFromTheUnion = dateOfWithdrawalFromTheUnion;
        this.groundsForWithdrawalFromTheUnion = groundsForWithdrawalFromTheUnion;
        this.isNotAMemberOfATradeUnion = isNotAMemberOfATradeUnion;
        this.directoryOfIndividualsEntity = directoryOfIndividualsEntity;
        this.departmentDirectoryEntity = departmentDirectoryEntity;
        this.directoryOfPrimaryTradeUnionOrganizations = directoryOfPrimaryTradeUnionOrganizations;
        this.directoryOfProfessionsEntity = directoryOfProfessionsEntity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getUnionTicketNumber() {
        return unionTicketNumber;
    }

    public void setUnionTicketNumber(long unionTicketNumber) {
        this.unionTicketNumber = unionTicketNumber;
    }

    public long getE_tradeUnionCard() {
        return e_tradeUnionCard;
    }

    public void setE_tradeUnionCard(long e_tradeUnionCard) {
        this.e_tradeUnionCard = e_tradeUnionCard;
    }

    public Date getDateOfJoiningTheUnion() {
        return dateOfJoiningTheUnion;
    }

    public void setDateOfJoiningTheUnion(Date dateOfJoiningTheUnion) {
        this.dateOfJoiningTheUnion = dateOfJoiningTheUnion;
    }

    public long getEPRN() {
        return EPRN;
    }

    public void setEPRN(long EPRN) {
        this.EPRN = EPRN;
    }

    public long getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(long officePhone) {
        this.officePhone = officePhone;
    }

    public Date getDateOfWithdrawalFromTheUnion() {
        return dateOfWithdrawalFromTheUnion;
    }

    public void setDateOfWithdrawalFromTheUnion(Date dateOfWithdrawalFromTheUnion) {
        this.dateOfWithdrawalFromTheUnion = dateOfWithdrawalFromTheUnion;
    }

    public String getGroundsForWithdrawalFromTheUnion() {
        return groundsForWithdrawalFromTheUnion;
    }

    public void setGroundsForWithdrawalFromTheUnion(String groundsForWithdrawalFromTheUnion) {
        this.groundsForWithdrawalFromTheUnion = groundsForWithdrawalFromTheUnion;
    }

    public boolean isNotAMemberOfATradeUnion() {
        return isNotAMemberOfATradeUnion;
    }

    public void setNotAMemberOfATradeUnion(boolean notAMemberOfATradeUnion) {
        isNotAMemberOfATradeUnion = notAMemberOfATradeUnion;
    }

    public DirectoryOfIndividualsEntity getDirectoryOfIndividualsEntity() {
        return directoryOfIndividualsEntity;
    }

    public void setDirectoryOfIndividualsEntity(DirectoryOfIndividualsEntity directoryOfIndividualsEntity) {
        this.directoryOfIndividualsEntity = directoryOfIndividualsEntity;
    }

    public DepartmentDirectoryEntity getDepartmentDirectoryEntity() {
        return departmentDirectoryEntity;
    }

    public void setDepartmentDirectoryEntity(DepartmentDirectoryEntity departmentDirectoryEntity) {
        this.departmentDirectoryEntity = departmentDirectoryEntity;
    }

    public DirectoryOfPrimaryTradeUnionOrganizations getDirectoryOfPrimaryTradeUnionOrganizations() {
        return directoryOfPrimaryTradeUnionOrganizations;
    }

    public void setDirectoryOfPrimaryTradeUnionOrganizations(DirectoryOfPrimaryTradeUnionOrganizations directoryOfPrimaryTradeUnionOrganizations) {
        this.directoryOfPrimaryTradeUnionOrganizations = directoryOfPrimaryTradeUnionOrganizations;
    }

    public DirectoryOfProfessionsEntity getDirectoryOfProfessionsEntity() {
        return directoryOfProfessionsEntity;
    }

    public void setDirectoryOfProfessionsEntity(DirectoryOfProfessionsEntity directoryOfProfessionsEntity) {
        this.directoryOfProfessionsEntity = directoryOfProfessionsEntity;
    }
}
