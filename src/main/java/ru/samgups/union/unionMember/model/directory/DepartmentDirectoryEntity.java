package ru.samgups.union.unionMember.model.directory;


import ru.samgups.union.unionMember.model.UnionMemberCard;

import javax.persistence.*;

// Справочник подразделений
@Entity
@Table(name = "DirectoryEntity")
public class DepartmentDirectoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id_department;
    @Column(name = "nameDepartment")
    private String nameDepartment;
    @Column(name = "firstLevel")
    private long firstLevel;
    @Column(name = "lastLevel")
    private long lastLevel;

    @OneToOne(optional = false, mappedBy = "departmentDirectoryEntity")
    @JoinColumn (name="departmentDirectoryEntityId")
    UnionMemberCard unionMemberCard;

    public DepartmentDirectoryEntity() {

    }

    public DepartmentDirectoryEntity(long id_department, String nameDepartment, long firstLevel, long lastLevel, UnionMemberCard unionMemberCard) {
        this.id_department = id_department;
        this.nameDepartment = nameDepartment;
        this.firstLevel = firstLevel;
        this.lastLevel = lastLevel;
        this.unionMemberCard = unionMemberCard;
    }

    public long getId_department() {
        return id_department;
    }

    public void setId_department(long id_department) {
        this.id_department = id_department;
    }

    public String getNameDepartment() {
        return nameDepartment;
    }

    public void setNameDepartment(String nameDepartment) {
        this.nameDepartment = nameDepartment;
    }

    public long getFirstLevel() {
        return firstLevel;
    }

    public void setFirstLevel(long firstLevel) {
        this.firstLevel = firstLevel;
    }

    public long getLastLevel() {
        return lastLevel;
    }

    public void setLastLevel(long lastLevel) {
        this.lastLevel = lastLevel;
    }

    public UnionMemberCard getUnionMemberCard() {
        return unionMemberCard;
    }

    public void setUnionMemberCard(UnionMemberCard unionMemberCard) {
        this.unionMemberCard = unionMemberCard;
    }
}
