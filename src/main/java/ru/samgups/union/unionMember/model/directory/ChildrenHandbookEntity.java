package ru.samgups.union.unionMember.model.directory;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

//Справочник детей
@Entity
@Table(name = "ChildrenHandbookEntity")
public class ChildrenHandbookEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id_child;
    @Column(name = "surname")
    private String surname;
    @Column(name = "name")
    private String name;
    @Column(name = "patronymic")
    private String patronymic;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @Column(name = "dateOfBirth")
    private Date dateOfBirth;

    @ManyToMany
    private List <DirectoryOfIndividualsEntity>directoryOfIndividualsEntities;

    public ChildrenHandbookEntity() {

    }

    public ChildrenHandbookEntity(int id_child, String surname, String name, String patronymic, Date dateOfBirth, List<DirectoryOfIndividualsEntity> directoryOfIndividualsEntities) {
        this.id_child = id_child;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.directoryOfIndividualsEntities = directoryOfIndividualsEntities;
    }

    public int getId_child() {
        return id_child;
    }

    public void setId_child(int id_child) {
        this.id_child = id_child;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<DirectoryOfIndividualsEntity> getDirectoryOfIndividualsEntities() {
        return directoryOfIndividualsEntities;
    }

    public void setDirectoryOfIndividualsEntities(List<DirectoryOfIndividualsEntity> directoryOfIndividualsEntities) {
        this.directoryOfIndividualsEntities = directoryOfIndividualsEntities;
    }
}
