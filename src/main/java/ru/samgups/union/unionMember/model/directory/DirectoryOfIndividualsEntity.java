package ru.samgups.union.unionMember.model.directory;


import org.springframework.format.annotation.DateTimeFormat;
import ru.samgups.union.unionMember.model.UnionMemberCard;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

//Cправочник физичиских лиц
@Entity
@Table(name = "OfIndividualsEntity")
public class DirectoryOfIndividualsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @Column(name = "surname")
    private String surname;
    @Column(name = "name")
    private String name;
    @Column(name = "patronymic")
    private String patronymic;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @Column(name = "dateOfBirth")
    private Date dateOfBirth;
    @Column(name = "gender")
    private boolean gender;
    @Column(name = "homeAddress")
    private String homeAddress;
    @Column(name = "homePhone")
    private long homePhone;
    @Column(name = "cellPhone")
    private long cellPhone;
    @Column(name = "eMail")
    private String eMail;
    @Column(name = "maritalStatus")
    private boolean MaritalStatus;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "idDirectoryOfIndividuals")
    private DirectoryEducationClassifierEntity directoryEducationClassifierEntity;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn (name="directoryOfIndividualsEntityId")
    UnionMemberCard unionMemberCard;


    @ManyToMany
    @JoinTable(
            name = "OfIndividuals_ChildrenHandbook",
            joinColumns = @JoinColumn(name = "OfIndividualsId", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "ChildrenHandbookId", referencedColumnName = "id"))
    private List<ChildrenHandbookEntity> childrenHandbookEntity;

    public DirectoryOfIndividualsEntity() {
    }

    public DirectoryOfIndividualsEntity(int id, String surname, String name, String patronymic, Date dateOfBirth, boolean gender, String homeAddress, long homePhone, long cellPhone, String eMail, boolean maritalStatus, DirectoryEducationClassifierEntity directoryEducationClassifierEntity, UnionMemberCard unionMemberCard, List<ChildrenHandbookEntity> childrenHandbookEntity) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.homeAddress = homeAddress;
        this.homePhone = homePhone;
        this.cellPhone = cellPhone;
        this.eMail = eMail;
        MaritalStatus = maritalStatus;
        this.directoryEducationClassifierEntity = directoryEducationClassifierEntity;
        this.unionMemberCard = unionMemberCard;
        this.childrenHandbookEntity = childrenHandbookEntity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        homeAddress = homeAddress;
    }

    public long getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(long homePhone) {
        homePhone = homePhone;
    }

    public long getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(long cellPhone) {
        cellPhone = cellPhone;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public boolean isMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(boolean maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    public DirectoryEducationClassifierEntity getDirectoryEducationClassifierEntity() {
        return directoryEducationClassifierEntity;
    }

    public void setDirectoryEducationClassifierEntity(DirectoryEducationClassifierEntity directoryEducationClassifierEntity) {
        this.directoryEducationClassifierEntity = directoryEducationClassifierEntity;
    }

    public List<ChildrenHandbookEntity> getChildrenHandbookEntity() {
        return childrenHandbookEntity;
    }

    public void setChildrenHandbookEntity(List<ChildrenHandbookEntity> childrenHandbookEntity) {
        this.childrenHandbookEntity = childrenHandbookEntity;
    }

    public UnionMemberCard getUnionMemberCard() {
        return unionMemberCard;
    }

    public void setUnionMemberCard(UnionMemberCard unionMemberCard) {
        this.unionMemberCard = unionMemberCard;
    }
}
