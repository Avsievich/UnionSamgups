package ru.samgups.union.unionMember.model.directory;


import javax.persistence.*;

// Справочник Классификатор Образования
@Entity
@Table(name = "EducationClassifierEntity")
public class DirectoryEducationClassifierEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;
    @OneToOne(optional = false, mappedBy = "directoryEducationClassifierEntity")
    @JoinColumn (name="directoryEducationClassifierEntityId")
    private DirectoryOfIndividualsEntity directoryOfIndividualsEntity;


    public DirectoryEducationClassifierEntity() {
    }

    public DirectoryEducationClassifierEntity(long id, String name, DirectoryOfIndividualsEntity directoryOfIndividualsEntity) {
        this.id = id;
        this.name = name;
        this.directoryOfIndividualsEntity = directoryOfIndividualsEntity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DirectoryOfIndividualsEntity getDirectoryOfIndividualsEntity() {
        return directoryOfIndividualsEntity;
    }

    public void setDirectoryOfIndividualsEntity(DirectoryOfIndividualsEntity directoryOfIndividualsEntity) {
        this.directoryOfIndividualsEntity = directoryOfIndividualsEntity;
    }
}
