package ru.samgups.union.unionMember.web.departmentDirectory;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class departmentDirectoryController {

    // Выводим страницу подразделений
    @RequestMapping(value = {"/unionmember/department"}, method = RequestMethod.GET)
    public String welcomePage(Model model) {

        model.addAttribute("title", "Система учета профсоюза");
        return "unionMember/departmentHandbook/departmentHandbookPage";
    }
}
