package ru.samgups.union.unionMember.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class inionMemberController {


    // Выводим стартовую страницу Системы учета членов профсоюза
    @RequestMapping(value = {"/unionmember"}, method = RequestMethod.GET)
    public String welcomePage(Model model) {

        model.addAttribute("title", "Система учета профсоюза");
        return "unionMember/inionPage";
    }
}
